//
//  DTTDateHelper.m
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 2/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import "DTTDateHelper.h"

@implementation DTTDateHelper

+(NSString*)formatLongDate:(NSDate *)date
{
    // Get the day of the week
    NSString *dayOfWeek = [self getDayOfWeekForDate:date];
    
    // Get today's date
    NSDate *today = [self todaysDate];
    
    // If today then return this with the time value
    if ([today isEqualToDate:[self dateOnlyFromDate:date]])
    {
        return [NSString stringWithFormat:@"Today   (%@)", dayOfWeek];
    }
    
    NSDate *yesterday = [NSDate dateWithTimeInterval:-(60.0*60.0*24.0) sinceDate:today];
    
    // If yesterday then return this with the time value
    if ([yesterday isEqualToDate:[self dateOnlyFromDate:date]])
    {
        return [NSString stringWithFormat:@"Yesterday   (%@)", dayOfWeek];
    }
    
    NSDate *tomorrow = [NSDate dateWithTimeInterval:(60.0*60.0*24.0) sinceDate:today];
    
    // If yesterday then return this with the time value
    if ([tomorrow isEqualToDate:[self dateOnlyFromDate:date]])
    {
        return [NSString stringWithFormat:@"Tomorrow   (%@)", dayOfWeek];
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    dateFormatter.timeStyle = NSDateFormatterNoStyle;
    
    return [NSString stringWithFormat:@"%@ %@", dayOfWeek,[dateFormatter stringFromDate:date]];
}

+(NSString *)formatLongDateNonRelative:(NSDate *)date
{
    // Get the day of the week
    NSString *dayOfWeek = [self getDayOfWeekForDate:date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    dateFormatter.timeStyle = NSDateFormatterNoStyle;
    
    return [NSString stringWithFormat:@"%@ %@", dayOfWeek,[dateFormatter stringFromDate:date]];
}

+(NSString*)formatTime:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateStyle = NSDateFormatterNoStyle;
    dateFormatter.timeStyle = NSDateFormatterShortStyle;
    
    return [dateFormatter stringFromDate:date];
}

+(NSString*)formatShortDateNonRelative:(NSDate *)date
{
    // Get the day of the week
    NSString *dayOfWeek = [self getDayOfWeekForDate:date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateStyle = NSDateFormatterShortStyle;
    dateFormatter.timeStyle = NSDateFormatterNoStyle;
    
    return [NSString stringWithFormat:@"%@ %@", [dayOfWeek substringToIndex:3],[dateFormatter stringFromDate:date]];
}


+(NSDate*)dateOnlyFromDate:(NSDate*) date
{
    unsigned int flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    NSDateComponents* components = [calendar components:flags fromDate:date];
    
    return [calendar dateFromComponents:components];
}

+(NSString*)getDayOfWeekForDate:(NSDate*)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = @"EEEE";
    
    return [dateFormatter stringFromDate:date];
}

+(NSDate*)getDateRelativeTo:(NSDate *)date adjustedByDays:(NSInteger)days
{
    NSInteger timeInterval = 60 * 60 * 24 * days;
    
    return [NSDate dateWithTimeInterval:timeInterval sinceDate:date];
}

+(BOOL)isDayAWeekend:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSRange weekdayRange = [calendar maximumRangeOfUnit:NSCalendarUnitWeekday];
    NSDateComponents *components = [calendar components:NSCalendarUnitWeekday fromDate:date];
    NSUInteger weekdayOfDate = [components weekday];
    
    if (weekdayOfDate == weekdayRange.location || weekdayOfDate == weekdayRange.length) {
        return YES;
    }
    
    return NO;
}

+(NSDate*)todaysDate
{
    return [self dateOnlyFromDate:[NSDate date]];
    // Test code below for changing today's date
    //return [self getDateRelativeTo:[self dateOnlyFromDate:[NSDate date]] adjustedByDays:2];
}

@end
