//
//  DTTTaskMoveOptionsController.m
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 12/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import "DTTTaskMoveOptionsController.h"
#import "DTTVisibleDatesCollection.h"
#import "DTTDateDataController.h"
#import <AskingPoint/AskingPoint.h>


@implementation DTTTaskMoveOptionsController

@synthesize delegate = _delegate;
@synthesize date = _date;
@synthesize task = _task;

-(void)showTaskMoveOptions:(DTTTask *)task forDate:(DTTDate *)date
{
    self.task = task;
    self.date = date;
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:task.description message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Move to next day", @"Copy to next day", nil];
    
    [alertView show];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        // Cancel
        return;
    }
    
    // Get the text date
    NSDate *nextDate = [DTTVisibleDatesCollection getNextDateAfter:self.date.date];
    DTTDate *toDate = [DTTDateDataController getDate:nextDate];
    
    if(buttonIndex == 1)
    {
        // Log the move
        [ASKPManager addEventWithName:@"MovedTaskToNextDay"];
        
        // Move
        // Remove the task from the old day
        [self.date deleteTask:self.task];
        
        // Add the task to the new date
        [toDate.tasks addObject:self.task];
        [DTTDateDataController saveDate:self.date];
        [DTTDateDataController saveDate:toDate];
    }
    
    if (buttonIndex == 2)
    {
        // Log the copy
        [ASKPManager addEventWithName:@"CopiedTaskToNextDay"];
        
        // Copy
        [toDate.tasks addObject:self.task];
        [DTTDateDataController saveDate:toDate];
    }
    
    // Call the delegate
    [self.delegate taskMoveOptionsComplete:self];
}

@end
