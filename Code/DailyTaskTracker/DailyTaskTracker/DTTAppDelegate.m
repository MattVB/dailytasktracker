//
//  DTTAppDelegate.m
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 1/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import "DTTAppDelegate.h"
#import "DTTDateDataController.h"
#import "MKICloudSync.h"
#import <AskingPoint/AskingPoint.h>

@implementation DTTAppDelegate

@synthesize  idleTimer = _idleTimer;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Initalise asking point
    //[ASKPManager startup:@"vgDXABsEUzwlMQ3MF2L67AbQ0yiDV1SdPvD0OU7C27k="]; // Test app key
    [ASKPManager startup:@"GgDhABsEUzx6AY3E6YbQKHCbfW-RGCOe0TLnYbphhAc="];
    
    // Initialise iCloud
    [self initialiseiCloud];
    
    // Override point for customization after application launch.
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    // Tell the Data controller that the app has restarted
    [DTTDateDataController applicationResumed];
    
    // Post notification that the application has resumed. This may be used by the UI to refresh the view
    [[NSNotificationCenter defaultCenter]postNotificationName:@"ApplicationResumed" object:nil];
    
    // Reset the idle timer
    [self resetIdleTimer];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (void)sendEvent:(UIEvent *)event {
    [super sendEvent:event];
    
    // Only want to reset the timer on a Began touch or an Ended touch, to reduce the number of timer resets.
    NSSet *allTouches = [event allTouches];
    if ([allTouches count] > 0) {
        // allTouches count only ever seems to be 1, so anyObject works here.
        UITouchPhase phase = ((UITouch *)[allTouches anyObject]).phase;
        if (phase == UITouchPhaseBegan || phase == UITouchPhaseEnded)
            [self resetIdleTimer];
    }
}

- (void)resetIdleTimer {
    if (self.idleTimer) {
        [self.idleTimer invalidate];
    }
    
    // Set the idle timeout
    self.idleTimerDisabled = YES;
    
    // Get the time
    NSInteger idleTimeMinutes = [[NSUserDefaults standardUserDefaults]integerForKey:@"idle_after_x_minutes"];
    
    // Use a default of 5 minutes
    if (idleTimeMinutes == 0)
    {
        idleTimeMinutes = 5;
    }
    
    // If value is one then we don't need to do anything
    if (idleTimeMinutes <= 1)
    {
        return;
    }
    
    // Take off one minute as that is how long till the phone stops responding using the standard idle timer
    NSTimeInterval idleTimeSeconds = 60 * (idleTimeMinutes -1);
    
    self.idleTimer = [NSTimer scheduledTimerWithTimeInterval:idleTimeSeconds target:self selector:@selector(idleTimerExceeded) userInfo:nil repeats:NO];
}

- (void)idleTimerExceeded {
    NSLog(@"idle time exceeded");
    
    // Set the idle timer enabled so that the app can go to sleep
    self.idleTimerDisabled = NO;
}

-(void)initialiseiCloud
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if ([[NSFileManager defaultManager]
             URLForUbiquityContainerIdentifier:nil] != nil)
            NSLog(@"iCloud is available\n");
        else
            NSLog(@"iCloud is not available!!\n");
    });
    
    
    // Start the iCloud sync
    [MKiCloudSync start];
}

@end
