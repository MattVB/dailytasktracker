//
//  DTTSelectDateViewController.h
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 10/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DTTSelectDateViewControllerDelegate;

@interface DTTSelectDateViewController : UIViewController

@property NSDate *date;
- (IBAction)done:(id)sender;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (weak, nonatomic) id <DTTSelectDateViewControllerDelegate> delegate;

@end

@protocol DTTSelectDateViewControllerDelegate <NSObject>
- (void)selectDateViewContollerDateSelected:(DTTSelectDateViewController*)controller dateSelected:(NSDate*)date;
@end
