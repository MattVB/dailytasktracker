//
//  DTTCalendarEventsAppender.h
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 2/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTTDate.h"

@interface DTTCalendarEventsAppender : NSObject

+(void)addCalendarEventsForDate:(DTTDate*)date;

@end
