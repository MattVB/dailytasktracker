//
//  DTTAddTaskViewController.h
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 2/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTTTask.h"
#import "DTTDate.h"

@protocol DTTAddTaskViewControllerDelegate;

@interface DTTAddTaskViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableViewOptions;
@property (weak, nonatomic) id <DTTAddTaskViewControllerDelegate> delegate;
@property (strong, nonatomic) DTTDate* date;
- (IBAction)save:(id)sender;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@end


@protocol DTTAddTaskViewControllerDelegate <NSObject>
- (void)addTaskViewControllerComplete:(DTTAddTaskViewController*)controller task:(DTTTask*)task date:(DTTDate*)date;

- (void)addTaskViewControllerCancel:(DTTAddTaskViewController*)controller;
@end