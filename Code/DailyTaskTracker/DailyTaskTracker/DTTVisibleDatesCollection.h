//
//  DTTVisibleDatesCollection.h
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 2/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DTTVisibleDatesCollection : NSObject

    @property NSMutableArray *dates;

    -(id)initRelativeToToday;

    -(id)initRelativeToDate:(NSDate*)date;

    +(NSDate*)getNextDateAfter:(NSDate*)date;

@end
