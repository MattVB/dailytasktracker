//
//  DTTDetailViewController.m
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 1/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import "DTTDetailViewController.h"
#import "DTTDateDataController.h"
#import "DTTControlHelper.h"
#import "DTTVisibleDatesCollection.h"
#import "DTTTaskMover.h"


@interface DTTDetailViewController ()
    @property DTTTaskMoveOptionsController *taskMoveOptionsController;
@end

@implementation DTTDetailViewController

@synthesize task = _task;
@synthesize textDescription;
@synthesize textViewExtendedDescription;
@synthesize  taskMoveOptionsController = _taskMoveOptionsController;


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // Format the text area
    [DTTControlHelper formatTextView:self.textViewExtendedDescription];
   
    // Set the field values
    self.textDescription.text = self.task.description;
    self.textViewExtendedDescription.text = self.task.extendedDescription;
    
    // Set the title
    self.navigationItem.title = [@"Task: " stringByAppendingString:self.task.description];
}

- (void)viewDidUnload
{
    [self setTextDescription:nil];
    [self setTextViewExtendedDescription:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)save:(id)sender
{
    // Update the task
    self.task.description = self.textDescription.text;
    self.task.extendedDescription = self.textViewExtendedDescription.text;
    
    // Save the date
    [DTTDateDataController saveDate:self.date];
    
    // Call the delegate
    [self.delegate detailViewControllerSaveComplete:self task:self.task date:self.date];
}

- (IBAction)displayTaskActions:(id)sender {
    
    // Show the display action controller
    self.taskMoveOptionsController = [[DTTTaskMoveOptionsController alloc]init];
    self.taskMoveOptionsController.delegate = self;
    [self.taskMoveOptionsController showTaskMoveOptions:self.task forDate:self.date];
}

-(void)taskMoveOptionsComplete:(DTTTaskMoveOptionsController *)controller
{
    // Call the delegate
    [self.delegate detailViewControllerMovementComplete:self task:self.task date:self.date];
}

- (IBAction)trashTask:(id)sender
{
    UIActionSheet *actionSheet;
    
    if (self.task.isRecurringCalendarEvent)
    {
            actionSheet = [[UIActionSheet alloc]initWithTitle:@"Delete" delegate:self cancelButtonTitle:@"Cancel"destructiveButtonTitle:@"Delete all Occurances" otherButtonTitles:@"Delete Task",nil];
    }
    else
    {
            actionSheet = [[UIActionSheet alloc]initWithTitle:@"Delete" delegate:self cancelButtonTitle:@"Cancel"destructiveButtonTitle:@"Delete Task" otherButtonTitles:nil];
    }

    
    // Show the action sheet from the button
    [actionSheet showFromBarButtonItem:sender animated:YES];
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
    if (self.task.isRecurringCalendarEvent && buttonIndex == 2)
    {
        // Cancel
        return;
    }
    
    if (!self.task.isRecurringCalendarEvent && buttonIndex == 1)
    {
        // Cancel
        return;
    }
    
    if (self.task.isRecurringCalendarEvent && buttonIndex == 0)
    {
        // Do the delete
        [DTTDateDataController deleteReoccurancesOfCalendarTask:self.task];
        
        // Call the delegate
        [self.delegate detailViewControllerDeletionComplete:self task:self.task date:self.date];
        
        return;
    }
    
    // Otherwise it is delete the task
    [self.date deleteTask:self.task];
    [DTTDateDataController saveDate:self.date];
    
    // Call the delegate
    [self.delegate detailViewControllerDeletionComplete:self task:self.task date:self.date];
}

@end
