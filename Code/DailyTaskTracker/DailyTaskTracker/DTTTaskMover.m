//
//  DTTTaskMover.m
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 9/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import "DTTTaskMover.h"

@implementation DTTTaskMover

+(void)moveTask:(DTTTask *)task fromDate:(DTTDate *)fromDate toDate:(DTTDate *)toDate
{
    // Remove the task from the old day
    [fromDate deleteTask:task];
    
    // Add the task to the new date
    [toDate.tasks addObject:task];
}

@end
