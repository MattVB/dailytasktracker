//
//  DTTTask.m
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 1/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import "DTTTask.h"

@implementation DTTTask

@synthesize description = _description;
@synthesize extendedDescription = _extendedDescription;
@synthesize calendarID = _calendarID;
@synthesize isRecurringCalendarEvent = _isRecurringCalendarEvent;

-(id)initWithDescription:(NSString *)inputDescription andExtendedDescription:(NSString *)inputExtendedDescription
{
    self = [super init];
    if (self) {
        self.description = inputDescription;
        self.extendedDescription = inputExtendedDescription;
        self.isRecurringCalendarEvent = NO;
        
        return self;
    }
    
    return nil;
}

-(id)initWithDescription:(NSString *)inputDescription andExtendedDescription:(NSString *)inputExtendedDescription andCalendarID:(NSString *)inputCalendarID isRecurring:(BOOL)isRecurring;
{
    self = [super init];
    if (self) {
        self.description = inputDescription;
        self.extendedDescription = inputExtendedDescription;
        self.calendarID = inputCalendarID;
        self.isRecurringCalendarEvent = isRecurring;
        
        return self;
    }
    
    return nil;
}

-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.description forKey:@"description"];
    [aCoder encodeObject:self.extendedDescription forKey:@"extendedDescription"];
    [aCoder encodeObject:self.calendarID forKey:@"calendarID"];
    [aCoder encodeBool:self.isRecurringCalendarEvent forKey:@"isRecurringCalendarEvent"];
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self.description = [aDecoder decodeObjectForKey:@"description"];
    self.extendedDescription = [aDecoder decodeObjectForKey:@"extendedDescription"];
    self.calendarID = [aDecoder decodeObjectForKey:@"calendarID"];
    self.isRecurringCalendarEvent = [aDecoder decodeBoolForKey:@"isRecurringCalendarEvent"];
    
    return self;
}

@end
