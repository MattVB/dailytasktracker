//
//  DTTDateDataController.m
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 1/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import "DTTDateDataController.h"
#import "DTTCalendarEventsAppender.h"

@implementation DTTDateDataController

static NSString *datesKeyName = @"Dates";
static NSString *ignoreCalendarIDsKeyName = @"IgnoreCalendarKeys";

+(NSMutableDictionary*) getLoadedDates
{
    static NSMutableDictionary* cache = nil;
    
    if (cache == nil)
    {
        cache = [[NSMutableDictionary alloc]init];
    }
    
    return cache;
}

+(void)applicationResumed
{
    [self clearLoadedDates];
}

+(void)clearLoadedDates
{
    // Clear the cache so that it will reload
    [[self getLoadedDates]removeAllObjects];
}

+(DTTDate*)getDate:(NSDate *)dateToRetrieve
{
    DTTDate *date = [[self getLoadedDates]objectForKey:dateToRetrieve];
    
    if (date)
    {
        // Date has been loaded - return loaded date
        return date;
    }

    // Load the date and return
    return [self loadDate:dateToRetrieve];
}

+(DTTDate*)loadDate:(NSDate *)dateToRetrieve
{
    DTTDate *result = [[DTTDateDataController getDictionaryOfDates]objectForKey:dateToRetrieve];
    
    if (!result)
    {
        result = [[DTTDate alloc]initWithDate:dateToRetrieve];
    }
    
    // Add in the calendar events
    [DTTCalendarEventsAppender addCalendarEventsForDate:result];
    
    // Add to the loaded dates
    [[self getLoadedDates]setObject:result forKey:dateToRetrieve];
    
    // Return
    return result;
}

+(void)saveDate:(DTTDate *)date
{
    // Get the dictionary
    NSMutableDictionary *dates = [DTTDateDataController getDictionaryOfDates];
    
    // Set the value for the date
    [dates setObject:date forKey:date.date];
    
    // Store the data
    [DTTDateDataController saveDictionaryOfDates:dates];
    
    // Remove the object from the loaded list
    [[self getLoadedDates]removeObjectForKey:date.date];
}

+(NSMutableDictionary*)getDictionaryOfDates
{
    NSData *data = [[NSUserDefaults standardUserDefaults]objectForKey:datesKeyName];
    
    if (data)
    {
        NSMutableDictionary *result = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        return result;
    }
    
    // Doesn't exist yet - create the dictionary
    return [[NSMutableDictionary alloc]init];
}

+(void)deleteReoccurancesOfCalendarTask:(DTTTask *)task
{
    // Go through each date and remove the item if it exists
    for(DTTDate *date in [[self getDictionaryOfDates]allValues])
    {
        // Remove the task(s)
        [date removeTaskWithCalendarID:task.calendarID];
        
        // Save the day back
        [self saveDate:date];
    }
    
    // Clear the local cache
    [self clearLoadedDates];
    
    // Add to ignore list
    [self addCalendarIDToIgnore:task.calendarID];
}

+(void)saveDictionaryOfDates:(NSMutableDictionary*)dictionaryOfDates
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dictionaryOfDates];
    [[NSUserDefaults standardUserDefaults]setObject:data forKey:datesKeyName];
}

+(void)addCalendarIDToIgnore:(NSString*)calendarID
{
    NSMutableArray *calendarIDs = [self getCalendarIDsToIgnore];
    
    // Add to the list
    [calendarIDs addObject:calendarID];
    
    // Store the list back
    [[NSUserDefaults standardUserDefaults]setObject:calendarIDs forKey:ignoreCalendarIDsKeyName];
}

+(NSMutableArray*)getCalendarIDsToIgnore
{
    NSMutableArray *result = [[NSUserDefaults standardUserDefaults]objectForKey:ignoreCalendarIDsKeyName];
    
    if (result)
    {
        return [[NSMutableArray alloc]initWithArray:result];
    }
    
    return [[NSMutableArray alloc]init];
}

@end
