//
//  DTTMasterViewController.h
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 1/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTTDetailViewController.h"
#import "DTTAddTaskViewController.h"
#import "DTTTaskMoveOptionsController.h"

@interface DTTMasterViewController : UITableViewController <DTTDetailViewControllerDelegate, DTTAddTaskViewControllerDelegate, UIAlertViewDelegate, DTTTaskMoveOptionsControllerDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *shareButtonItem;

- (IBAction)taskLongPress:(id)sender;
- (IBAction)share:(id)sender;

@end
