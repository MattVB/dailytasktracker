//
//  DTTCollectionsHelper.m
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 2/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import "DTTCollectionsHelper.h"

@implementation DTTCollectionsHelper

+ (void)moveObjectInArray:(NSMutableArray*)array fromIndex:(NSUInteger)from toIndex:(NSUInteger)to
{
    if (to != from) {
        id obj = [array objectAtIndex:from];
        [array removeObjectAtIndex:from];
        if (to >= [array count]) {
            [array addObject:obj];
        } else{
            [array insertObject:obj atIndex:to];
        }
    }
}

@end
