//
//  DTTAddTaskViewController.m
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 2/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import "DTTAddTaskViewController.h"
#import "DTTDateDataController.h"
#import "DTTPreviousTasksListProvider.h"
#import <AskingPoint/AskingPoint.h>

@interface DTTAddTaskViewController ()
    @property NSArray* availableItems;
    @property NSArray* filteredtems;
@end

@implementation DTTAddTaskViewController
@synthesize tableViewOptions = _tableViewOptions;

@synthesize date = _date;
@synthesize availableItems = _availableItems;
@synthesize filteredtems = _filteredtems;
@synthesize searchBar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    // Change the "Seach" button on the keyboard to "Done".
    for (UIView *searchBarSubview in [self.searchBar subviews]) {
        
        if ([searchBarSubview conformsToProtocol:@protocol(UITextInputTraits)]) {
            
            @try {
                
                [(UITextField *)searchBarSubview setReturnKeyType:UIReturnKeyDone];
                [(UITextField *)searchBarSubview setKeyboardAppearance:UIKeyboardAppearanceAlert];
            }
            @catch (NSException * e) {
                
                // ignore exception
            }
        }
    }
}


- (void)viewDidUnload
{
    [self setSearchBar:nil];
    [self setTableViewOptions:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Setup the data
    self.availableItems = [DTTPreviousTasksListProvider getPreviousTasksList:self.date.date];
    self.filteredtems = [[NSArray alloc]init];
    
    // Put the focus into the field
    [self.searchBar becomeFirstResponder];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)save:(id)sender
{
    [self saveTask:self.searchBar.text];
}

-(void)saveTask:(NSString*)taskDescription
{
    // Create the new task
    DTTTask *task = [[DTTTask alloc]initWithDescription:taskDescription andExtendedDescription:@""];
    
    // Add the task to the list
    [self.date.tasks addObject:task];
    
    // Save the date
    [DTTDateDataController saveDate:self.date];
    
    // Log the Add
    [ASKPManager addEventWithName:@"AddedTask"];
    
    // Call the delegate
    [self.delegate addTaskViewControllerComplete:self task:task date:self.date];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.filteredtems.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OptionCell"];
    
    cell.textLabel.text = [self.filteredtems objectAtIndex:indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *selectedTask = [self.filteredtems objectAtIndex:indexPath.row];
    [self saveTask:selectedTask];
}

-(void)refreshItemsList:(NSString*)searchText
{
    // Filter the list
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@" SELF CONTAINS[cd] %@ ", searchText];
    self.filteredtems = [self.availableItems filteredArrayUsingPredicate:predicate];
    
    // Refresh the table
    [self.tableViewOptions reloadData];
}


-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self refreshItemsList:searchText];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    // Save and complete
    [self saveTask:self.searchBar.text];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    // Call the delegate
    [self.delegate addTaskViewControllerCancel:self];
}

@end
