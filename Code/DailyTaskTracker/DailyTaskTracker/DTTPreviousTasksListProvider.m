//
//  DTTPreviousTasksListProvider.m
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 3/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import "DTTPreviousTasksListProvider.h"
#import "DTTDateHelper.h"
#import "DTTDateDataController.h"
#import "DTTDate.h"
#import "DTTTask.h"

@implementation DTTPreviousTasksListProvider

+(NSArray*)getPreviousTasksList:(NSDate*)relativeDate
{
    NSMutableArray *result = [[NSMutableArray alloc]init];
    
    // Add the date's tasks to the list
    [self addTasksForDate:relativeDate toArray:result];
    
    // Add in tasks for one week forward and back
    for(NSInteger i = 1; i <= 7; i++)
    {
        // Add in day back
        [self addTasksForDate:[DTTDateHelper getDateRelativeTo:relativeDate adjustedByDays:-i] toArray:result];
        
        // Add in day forward
        [self addTasksForDate:[DTTDateHelper getDateRelativeTo:relativeDate adjustedByDays:i] toArray:result];
    }
    
    return result;
}
                     
+(void)addTasksForDate:(NSDate*)date toArray:(NSMutableArray*)array
{
    for(DTTTask *task in [DTTDateDataController getDate:date].tasks)
    {
        if (![array containsObject:task.description])
        {
            [array addObject:task.description];
        }
    }
}

@end
