//
//  DTTSingleDayViewController.m
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 10/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import "DTTSingleDayViewController.h"
#import "DTTDateHelper.h"
#import "DTTDate.h"
#import "DTTTask.h"
#import "DTTDateDataController.h"
#import "DTTCollectionsHelper.h"
#import "DTTDeviceHelper.h"


@interface DTTSingleDayViewController ()
    @property NSDate *currentDate;
    @property DTTTaskMoveOptionsController *taskMoveOptionsController;
    @property UIPopoverController *popoverController;
@end

@implementation DTTSingleDayViewController
@synthesize tableView;
@synthesize labelDate;
@synthesize  taskMoveOptionsController = _taskMoveOptionsController;
@synthesize popoverController = __popoverController;

@synthesize currentDate = _currentDate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // If no date is set then default to today
    if (!self.currentDate)
    {
         self.currentDate = [DTTDateHelper dateOnlyFromDate:[NSDate date]];
    }
    
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    self.editButtonItem.target = self;
    
    
    // Add in the tap gesture recogniser
    UITapGestureRecognizer *tapGestureRecognsier = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changeDate:)];
    [self.labelDate addGestureRecognizer:tapGestureRecognsier];
    
    // Add in the swipe left to move to the next day
    UISwipeGestureRecognizer *swipeNextDayGestureRecogniser = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(nextDate:)];
    swipeNextDayGestureRecogniser.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.labelDate addGestureRecognizer:swipeNextDayGestureRecogniser];
    
    // Add in the swipe right to move to the previous day
    UISwipeGestureRecognizer *swipePreviousDayGestureRecogniser = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(previousDate:)];
    swipePreviousDayGestureRecogniser.direction = UISwipeGestureRecognizerDirectionRight;
    [self.labelDate addGestureRecognizer:swipePreviousDayGestureRecogniser];
    
    // Add in the long press gesture recogniser for displaying the quick menu
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(taskLongPress:)];
    lpgr.minimumPressDuration = 1.5; //seconds
    [self.tableView addGestureRecognizer:lpgr];
    
    // Refresh the date
    [self refreshDateDisplay];
}

-(void)nextDate:(UISwipeGestureRecognizer*)gesture
{
    if(gesture.state != UIGestureRecognizerStateRecognized)
    {
        return;
    }
    
    // Move the date forward by one day
    self.currentDate = [DTTDateHelper getDateRelativeTo:self.currentDate adjustedByDays:1];
    [self refreshDateDisplay];
}

-(void)previousDate:(UISwipeGestureRecognizer*)gesture
{
    if(gesture.state != UIGestureRecognizerStateRecognized)
    {
        return;
    }
    
    // Move the date back by one day
    self.currentDate = [DTTDateHelper getDateRelativeTo:self.currentDate adjustedByDays:-1];
    [self refreshDateDisplay];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Refresh the table
    [self.tableView reloadData];
}

-(void)refreshDateDisplay
{
    // Set the label text
    self.labelDate.text = [DTTDateHelper formatLongDateNonRelative:self.currentDate];
    
    // Set the navigation text
    self.navigationItem.title = [DTTDateHelper formatShortDateNonRelative:self.currentDate];
    
    // Refresh the table
    [self.tableView reloadData];
}

- (void)viewDidUnload
{
    [self setLabelDate:nil];
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Get the date
    DTTDate *date = [DTTDateDataController getDate:self.currentDate];
    
    return date.tasks.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Get the date
    DTTDate *date = [DTTDateDataController getDate:self.currentDate];
    
    // If this is the last item then add as the "Add" cell
    if (indexPath.row >= date.tasks.count)
    {
        return [self.tableView dequeueReusableCellWithIdentifier:@"AddCell"];
    }
    
    // Get the task
    DTTTask *task = [date.tasks objectAtIndex:indexPath.row];
    
    // Check if there is extended description
    if ([task.extendedDescription isEqualToString:@""])
    {
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"TitleOnlyCell"];
        cell.textLabel.text = task.description;
        return cell;
        
    }
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ExtendedDescriptionCell"];
    cell.textLabel.text = task.description;
    cell.detailTextLabel.text = task.extendedDescription;
    return cell;
}


- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    [self.tableView setEditing:editing animated:YES];
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Get the day
    DTTDate *date = [DTTDateDataController getDate:self.currentDate];
    
    return indexPath.row < date.tasks.count;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Get the day
        DTTDate *date = [DTTDateDataController getDate:self.currentDate];
        
        // Remove the task
        [date deleteTask:[date.tasks objectAtIndex:indexPath.row]];
        
        // Save the date
        [DTTDateDataController saveDate:date];
        
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}


// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    // Handle a move within a day
    if (fromIndexPath.section == toIndexPath.section)
    {
        // Get the date
        DTTDate *date = [DTTDateDataController getDate:self.currentDate];
        
        // Move the item
        [DTTCollectionsHelper moveObjectInArray:date.tasks fromIndex:fromIndexPath.row toIndex:toIndexPath.row];
        
        // Save the date
        [DTTDateDataController saveDate:date];
        
        return;
    }
}

- (IBAction)changeDate:(UIGestureRecognizer*)gesture
{
    if(gesture.state != UIGestureRecognizerStateRecognized)
    {
        return;
    }
    
    [self performSegueWithIdentifier:@"SelectDate" sender:gesture];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the destination view controller
    id destinationViewController = [segue destinationViewController];
    
    // If this is a navigation view controller then drill through to the first view controller to be actually displayed
    if ([destinationViewController isKindOfClass:[UINavigationController class]])
    {
        destinationViewController = [[destinationViewController viewControllers] objectAtIndex:0];
    }
    
    // Check for if this is a popover and if so - hold onto the popover controller so that we can dismiss later
    if ([segue isKindOfClass:[UIStoryboardPopoverSegue class]])
    {
        self.popoverController = ((UIStoryboardPopoverSegue*)segue).popoverController;
    }
    
    if ([segue.identifier isEqualToString:@"SelectDate"])
    {
        // Set the date on the destination view controller.
        DTTSelectDateViewController *selectDateViewController = segue.destinationViewController;
        selectDateViewController.date = self.currentDate;
        selectDateViewController.delegate = self;
    }
    
    if ([[segue identifier] hasPrefix:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        // Get the selected task
        DTTDate *dateObject = [DTTDateDataController getDate:self.currentDate];
        DTTTask *task = [dateObject.tasks objectAtIndex:indexPath.row];
        
        // Set the properties
        DTTDetailViewController *destinationViewController = (DTTDetailViewController*)segue.destinationViewController;
        destinationViewController.task = task;
        destinationViewController.date = dateObject;
        
        // Set delegate
        destinationViewController.delegate = self;
    }
    
    if ([[segue identifier] isEqualToString:@"addTask"]) {
        // Get the selected task
        DTTDate *dateObject = [DTTDateDataController getDate:self.currentDate];
        
        // Set the properties
        DTTAddTaskViewController *destinationViewController = (DTTAddTaskViewController*)segue.destinationViewController;
        destinationViewController.date = dateObject;
        
        // Set delegate
        destinationViewController.delegate = self;
    }

}

-(void)selectDateViewContollerDateSelected:(DTTSelectDateViewController *)controller dateSelected:(NSDate *)date
{
    // Set the date
    self.currentDate = date;
    
    // Refresh the display
    [self refreshDateDisplay];
    
    // Pop the controller
    [self.navigationController popViewControllerAnimated:YES];
    
    // Close the popover
    [self.popoverController dismissPopoverAnimated:YES];
}


-(void)detailViewControllerSaveComplete:(DTTDetailViewController *)controller task:(DTTTask *)task date:(DTTDate *)date
{
    // Pop the view controller
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    // Refresh the table
    [self.tableView reloadData];
}

-(void)detailViewControllerDeletionComplete:(DTTDetailViewController *)controller task:(DTTTask *)task date:(DTTDate *)date
{
    // Pop the view controller
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    // Refresh the table
    [self.tableView reloadData];
}

-(void)detailViewControllerMovementComplete:(DTTDetailViewController *)controller task:(DTTTask *)task date:(DTTDate *)date
{
    // Pop the view controller
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    // Refresh the table
    [self.tableView reloadData];
}

-(void)addTaskViewControllerComplete:(DTTAddTaskViewController *)controller task:(DTTTask *)task date:(DTTDate *)date
{
    // Pop the view controller
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    // Close the popover
    [self.popoverController dismissPopoverAnimated:YES];
    
    // Refresh the table
    [self.tableView reloadData];
}

- (void)addTaskViewControllerCancel:(DTTAddTaskViewController*)controller
{
    // Pop the view controller
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    // Close the popover
    [self.popoverController dismissPopoverAnimated:YES];
    
    // Refresh the table
    [self.tableView reloadData];
}

- (IBAction)displayDateHelp:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Date Help" message:@"To change date tap the date. To move back or forward by one day swipe across the date." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alertView show];
}

- (IBAction)nextDay:(id)sender
{
    // Move the date back by one day
    self.currentDate = [DTTDateHelper getDateRelativeTo:self.currentDate adjustedByDays:1];
    [self refreshDateDisplay];
}

- (IBAction)previousDay:(id)sender
{
    // Move the date back by one day
    self.currentDate = [DTTDateHelper getDateRelativeTo:self.currentDate adjustedByDays:-1];
    [self refreshDateDisplay];
}

- (IBAction)taskLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if(gestureRecognizer.state != UIGestureRecognizerStateRecognized)
    {
        return;
    }
    
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    if (indexPath == nil)
    {
        return;
    }
    
    // Get the selected task
    DTTDate *date = [DTTDateDataController getDate:self.currentDate];
    
    if (indexPath.row >= date.tasks.count)
    {
        // Didn't select a valid task
        return;
    }
    
    DTTTask *task = [date.tasks objectAtIndex:indexPath.row];
    
    // Show the display action controller
    self.taskMoveOptionsController = [[DTTTaskMoveOptionsController alloc]init];
    self.taskMoveOptionsController.delegate = self;
    [self.taskMoveOptionsController showTaskMoveOptions:task forDate:date];
}

-(void)taskMoveOptionsComplete:(DTTTaskMoveOptionsController *)controller
{
    
    // Refresh the data
    [self.tableView reloadData];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Don't do this for iPhones
    if (![DTTDeviceHelper isIpad])
    {
        return;
    }
    
    // Get the question response
    DTTDate *date = [DTTDateDataController getDate:self.currentDate];
    
    // Don't do if this is a proper row
    if (indexPath.row < date.tasks.count)
    {
        return;
    }
    
    // Create the view controller
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPadStoryboard"
                                                             bundle: nil];
    DTTAddTaskViewController *viewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"AddTask"];
    viewController.date = date;
    viewController.delegate = self;
    
    // Create popover and display
    self.popoverController = [[UIPopoverController alloc]
                              initWithContentViewController:viewController];
    self.popoverController.popoverContentSize = CGSizeMake(350, 500);
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    [self.popoverController presentPopoverFromRect:cell.bounds inView:cell.contentView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
}


- (IBAction)share:(id)sender
{
    DTTDate *dateObject = [DTTDateDataController getDate:self.currentDate];
    
    // Generate a string for each day
    NSArray *items = [[NSArray alloc]initWithObjects:[dateObject generateDescription], nil];

    // Present the view controller
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
    
    // Display the popover appropriately - Popover for iPad, modally for other devices
    if ([DTTDeviceHelper isIpad])
    {
        // Create popover and display
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:activityViewController];
        self.popoverController.popoverContentSize = CGSizeMake(380, 260);
        [self.popoverController presentPopoverFromBarButtonItem:self.shareButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else
    {
        [self presentViewController:activityViewController animated:YES completion:nil];
        
    }
}
@end
