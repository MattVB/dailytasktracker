//
//  DTTSelectDateViewController.m
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 10/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import "DTTSelectDateViewController.h"

@interface DTTSelectDateViewController ()

@end

@implementation DTTSelectDateViewController
@synthesize datePicker;
@synthesize date = _date;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Set the date
    self.datePicker.date = self.date;
}

- (void)viewDidUnload
{
    [self setDatePicker:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)done:(id)sender
{
    // Call the delegate
    [self.delegate selectDateViewContollerDateSelected:self dateSelected:self.datePicker.date];
}
@end
