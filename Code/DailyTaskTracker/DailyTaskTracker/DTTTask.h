//
//  DTTTask.h
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 1/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DTTTask : NSObject <NSCoding>

    @property (nonatomic, copy) NSString *description;
    @property (nonatomic, copy) NSString *extendedDescription;

    @property (nonatomic, copy) NSString *calendarID;
    @property (nonatomic) BOOL isRecurringCalendarEvent;

    -(id)initWithDescription:(NSString*)inputDescription andExtendedDescription:(NSString*)inputExtendedDescription;

-(id)initWithDescription:(NSString*)inputDescription andExtendedDescription:(NSString*)inputExtendedDescription andCalendarID:(NSString*)inputCalendarID isRecurring:(BOOL)isRecurring;;

@end
