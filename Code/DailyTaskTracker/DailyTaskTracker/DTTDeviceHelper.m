//
//  DTTDeviceHelper.m
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 15/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import "DTTDeviceHelper.h"

@implementation DTTDeviceHelper

+(BOOL)isIpad
{
    NSString *device = [UIDevice currentDevice].model;
    
    return [device hasPrefix:@"iPad"];
}

@end
