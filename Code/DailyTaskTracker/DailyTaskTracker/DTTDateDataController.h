//
//  DTTDateDataController.h
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 1/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTTDate.h"
#import "DTTTask.h"

@interface DTTDateDataController : NSObject

    +(DTTDate*)getDate:(NSDate*)dateToRetrieve;

    +(void)saveDate:(DTTDate*)date;

    +(void)applicationResumed;

    +(void)deleteReoccurancesOfCalendarTask:(DTTTask*)task;

    +(NSMutableArray*)getCalendarIDsToIgnore;

@end
