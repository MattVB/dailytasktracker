//
//  DTTVisibleDatesCollection.m
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 2/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import "DTTVisibleDatesCollection.h"
#import "DTTDateHelper.h"

@implementation DTTVisibleDatesCollection

@synthesize dates = _dates;

-(id)initRelativeToToday
{
    return [self initRelativeToDate:[NSDate date]];
}

-(id)initRelativeToDate:(NSDate *)date
{
    self = [super init];
    if (self) {
        self.dates = [[NSMutableArray alloc]init];
        
        // Set the initial date
        [self addNextDate:date];
        
        // Add in date before
        [self addPreviousDate];
        
        // Add in the day after
        [self addNextDate];
        
        return self;
    }
    
    return nil;

}

-(void)addPreviousDate
{
    // Get the first date
    NSDate *firstDate = [self.dates objectAtIndex:0];
    
    // Get the adjusted date
    NSDate *date = [DTTDateHelper getDateRelativeTo:firstDate adjustedByDays:-1];
    
    if (![DTTVisibleDatesCollection includeWeekends])
    {
        // Deal with the weekend
        while ([DTTDateHelper isDayAWeekend:date])
        {
            date = [DTTDateHelper getDateRelativeTo:date adjustedByDays:-1];
        }
    }
    
    // Add to the list at the start
    [self.dates insertObject:date atIndex:0];
}

-(void)addNextDate
{
    NSDate *date;
    
    if (self.dates.count == 0)
    {
        // No dates yet, so use today's date
        date = [DTTDateHelper dateOnlyFromDate:[NSDate date]];
    }
    else
    {
        // Get the last date
        NSDate *lastDate = [self.dates objectAtIndex:self.dates.count - 1];
        
        // Add a date on
        date = [DTTDateHelper getDateRelativeTo:lastDate adjustedByDays:1];
    }

    // Finish off the add
    [self addNextDate:date];
}

-(void)addNextDate:(NSDate*)date
{
    if (![DTTVisibleDatesCollection includeWeekends])
    {
        // Deal with the weekend
        while ([DTTDateHelper isDayAWeekend:date])
        {
            date = [DTTDateHelper getDateRelativeTo:date adjustedByDays:1];
        }
    }
    
    // Add to the list at the start
    [self.dates addObject:date];
}

+(NSDate*)getNextDateAfter:(NSDate *)date
{
    NSDate *result = [DTTDateHelper getDateRelativeTo:date adjustedByDays:1];
    
    if (![self includeWeekends])
    {
        // Deal with the weekend
        while ([DTTDateHelper isDayAWeekend:result])
        {
            result = [DTTDateHelper getDateRelativeTo:result adjustedByDays:1];
        }
    }
    
    return result;
}

+(BOOL)includeWeekends
{
    return [[NSUserDefaults standardUserDefaults]boolForKey:@"include_weekends"];
}

@end
