//
//  DTTTaskMover.h
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 9/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTTTask.h"
#import "DTTDate.h"

@interface DTTTaskMover : NSObject

+(void)moveTask:(DTTTask*)task fromDate:(DTTDate*)fromDate toDate:(DTTDate*)toDate;

@end
