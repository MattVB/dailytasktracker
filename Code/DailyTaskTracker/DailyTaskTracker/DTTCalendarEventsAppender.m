//
//  DTTCalendarEventsAppender.m
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 2/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import "DTTCalendarEventsAppender.h"
#import <EventKit/EKEventStore.h>
#import <EventKit/EKEvent.h>
#import "DTTTask.h"
#import "DTTDateDataController.h"
#import "DTTDateHelper.h"

@implementation DTTCalendarEventsAppender

+(EKEventStore*) getEventStore
{
    static EKEventStore* store = nil;
    
    if (store == nil)
    {
        store = [[EKEventStore alloc] init];
    }
    
    return store;
}

+(void)addCalendarEventsForDate:(DTTDate *)date
{
    // Get the current calendar
    EKEventStore *store = [self getEventStore];
    
    /* iOS 6 requires the user grant your application access to the Event Stores */
    if ([store respondsToSelector:@selector(requestAccessToEntityType:completion:)])
    {
        /* iOS Settings > Privacy > Calendars > MY APP > ENABLE | DISABLE */
        [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
         {
             if ( granted )
             {
                 NSLog(@"User has granted permission!");
             }
             else
             {
                 NSLog(@"User has not granted permission!");
             }
         }];
    }
    
    // Get the following date
     NSDate *endDate = [NSDate dateWithTimeInterval:(60.0*60.0*24.0) sinceDate:date.date];
    
    // Create the predicate from the event store's instance method
    NSPredicate *predicate = [store predicateForEventsWithStartDate:date.date
                                                            endDate:endDate
                                                          calendars:nil];
    
    // Fetch all events that match the predicate
    NSArray *events = [store eventsMatchingPredicate:predicate];
    
    for(EKEvent *event in events)
    {
        if ([self shouldAddTaskForEvent:event onDate:date])
        {
            NSString *extendedDescription;
            
            NSString *location = @"";
            
            // Handle the fact that location might be null
            if (!event.location)
            {
                location = event.location;
            }
            
            if (event.isAllDay)
            {
                extendedDescription = [NSString stringWithFormat:@"All Day  %@", location];
            }
            else
            {
                // Start time - end time location
                extendedDescription = [NSString stringWithFormat:@"%@ - %@  %@", [DTTDateHelper formatTime:event.startDate], [DTTDateHelper formatTime:event.endDate], location];
            }
            
            // Create task and add
            DTTTask *task = [[DTTTask alloc]initWithDescription:event.title andExtendedDescription:extendedDescription andCalendarID:event.eventIdentifier isRecurring:event.hasRecurrenceRules];
        
            [date.tasks addObject:task];
        }
    }
}

+(BOOL)shouldAddTaskForEvent:(EKEvent*)event onDate:(DTTDate*)date
{
    // Check for if the calendar event has already been deleted
    if ([date.deletedCalendarEventIDs containsObject:event.eventIdentifier])
    {
        return NO;
    }

    
    // Check for if a task already exists for this event.
    for(DTTTask *task in date.tasks)
    {
        if ([task.calendarID isEqualToString:event.eventIdentifier])
        {
            return NO;
        }
    }
    
    
    // Check for if the calendar event is in the ignore list
    if ([[DTTDateDataController getCalendarIDsToIgnore]containsObject:event.eventIdentifier]) {
        return NO;
    }
    
    return YES;
}

@end
