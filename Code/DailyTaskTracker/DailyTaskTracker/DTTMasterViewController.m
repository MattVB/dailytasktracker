//
//  DTTMasterViewController.m
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 1/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import "DTTMasterViewController.h"
#import "DTTDetailViewController.h"
#import "DTTDate.h"
#import "DTTTask.h"
#import "DTTDateDataController.h"
#import "DTTDateHelper.h"
#import "DTTCollectionsHelper.h"
#import "DTTVisibleDatesCollection.h"
#import "DTTTaskMover.h"
#import "DTTDeviceHelper.h"
#import <AskingPoint/AskingPoint.h>

@interface DTTMasterViewController () {

}
    @property DTTVisibleDatesCollection *visibleDates;
    @property NSIndexPath *actioningItemIndexPath;
    @property DTTTaskMoveOptionsController *taskMoveOptionsController;
    @property UIPopoverController *popoverController;

@end

@implementation DTTMasterViewController

@synthesize visibleDates = _visibleDates;
@synthesize actioningItemIndexPath = _actioningItemIndexPath;
@synthesize taskMoveOptionsController = _taskMoveOptionsController;
@synthesize popoverController = __popoverViewController;

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Setup the days to be displayed
    [self createVisibleDateCollection];
    
	// Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    // Add in the long press gesture recogniser for displaying the quick menu
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(taskLongPress:)];
    lpgr.minimumPressDuration = 1.5; //seconds
    [self.tableView addGestureRecognizer:lpgr];
    
    // Add in listner for notification on app resumption
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(applicationResumed) name:@"ApplicationResumed" object:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Refresh the table
    [self.tableView reloadData];
}


-(void)applicationResumed
{
    // Reload the visible dates collection
    [self createVisibleDateCollection];
    
    // Refresh the UI
    [self.tableView reloadData];
}

-(void)createVisibleDateCollection
{
    // Setup the days to be displayed
    NSDate *relativeDate = [DTTDateHelper dateOnlyFromDate:[NSDate date]];
    // Test code below for changing today's date
    //NSDate *relativeDate = [DTTDateHelper getDateRelativeTo:[DTTDateHelper dateOnlyFromDate:[NSDate date]] adjustedByDays:2];
    self.visibleDates = [[DTTVisibleDatesCollection alloc]initRelativeToDate:relativeDate];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.visibleDates.dates.count;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    // Get the selected date
    NSDate *selectedDate = [self.visibleDates.dates objectAtIndex:section];
    
    return [DTTDateHelper formatLongDate:selectedDate];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Get the selected date
    DTTDate *date = [self getDateForIndex:section];
    
    return date.tasks.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Get the date
    DTTDate *dateObject = [self getDateForIndex:indexPath.section];
    
    // If this is the last item then add as the "Add" cell
    if (indexPath.row >= dateObject.tasks.count)
    {
        return [tableView dequeueReusableCellWithIdentifier:@"AddCell"];
    }
    
    // Get the task
    DTTTask *task = [dateObject.tasks objectAtIndex:indexPath.row];
    
    // Check if there is extended description
    if ([task.extendedDescription isEqualToString:@""])
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TitleOnlyCell"];
        cell.textLabel.text = task.description;
        return cell;

    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ExtendedDescriptionCell"];
    cell.textLabel.text = task.description;
    cell.detailTextLabel.text = task.extendedDescription;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Get the day
    DTTDate *date = [self getDateForIndex:indexPath.section];
    
    return indexPath.row < date.tasks.count;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Get the day
        DTTDate *date = [self getDateForIndex:indexPath.section];
        
        // Remove the task
        [date deleteTask:[date.tasks objectAtIndex:indexPath.row]];
        
        // Save the date
        [DTTDateDataController saveDate:date];

        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        // Log the delete
        [ASKPManager addEventWithName:@"DeletedTask"];
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}


// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    // Log the move
    [ASKPManager addEventWithName:@"MovedTask"];
    
    // Handle a move within a day
    if (fromIndexPath.section == toIndexPath.section)
    {
        // Get the date
        DTTDate *date = [self getDateForIndex:fromIndexPath.section];
        
        // Move the item
        [DTTCollectionsHelper moveObjectInArray:date.tasks fromIndex:fromIndexPath.row toIndex:toIndexPath.row];
        
        // Save the date
        [DTTDateDataController saveDate:date];
        
        return;
    }
    
    // Handle between days
    DTTDate *fromDate = [self getDateForIndex:fromIndexPath.section];
    DTTDate *toDate = [self getDateForIndex:toIndexPath.section];
    
    // Get the object
    DTTTask *task = [fromDate.tasks objectAtIndex:fromIndexPath.row];
    
    // Remove the object
    [fromDate.tasks removeObject:task];
    
    if (toIndexPath.row >= toDate.tasks.count)
    {
        [toDate.tasks addObject:task];
    }
    else
    {
        [toDate.tasks insertObject:task atIndex:toIndexPath.row];
    }
    
    // Save both dates back
    [DTTDateDataController saveDate:fromDate];
    [DTTDateDataController saveDate:toDate];
}


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([[segue identifier] hasPrefix:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        // Get the selected task
        DTTDate *dateObject = [self getDateForIndex:indexPath.section];
        DTTTask *task = [dateObject.tasks objectAtIndex:indexPath.row];
       
        // Set the properties
        DTTDetailViewController *destinationViewController = (DTTDetailViewController*)segue.destinationViewController;
        destinationViewController.task = task;
        destinationViewController.date = dateObject;
        
        // Set delegate
        destinationViewController.delegate = self;
    }
    
    if ([[segue identifier] isEqualToString:@"addTask"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        // Get the selected task
        DTTDate *dateObject = [self getDateForIndex:indexPath.section];
        
        // Set the properties
        DTTAddTaskViewController *destinationViewController = (DTTAddTaskViewController*)segue.destinationViewController;
        destinationViewController.date = dateObject;
        
        // Set delegate
        destinationViewController.delegate = self;
    }
}

-(void)detailViewControllerSaveComplete:(DTTDetailViewController *)controller task:(DTTTask *)task date:(DTTDate *)date
{
    // Pop the view controller
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    // Refresh the table
    [self.tableView reloadData];
}

-(void)detailViewControllerDeletionComplete:(DTTDetailViewController *)controller task:(DTTTask *)task date:(DTTDate *)date
{
    // Pop the view controller
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    // Refresh the table
    [self.tableView reloadData];
}

-(void)detailViewControllerMovementComplete:(DTTDetailViewController *)controller task:(DTTTask *)task date:(DTTDate *)date
{
    // Pop the view controller
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    // Refresh the table
    [self.tableView reloadData];
}

-(void)addTaskViewControllerComplete:(DTTAddTaskViewController *)controller task:(DTTTask *)task date:(DTTDate *)date
{
    // Pop the view controller
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    // Close the popover
    [self.popoverController dismissPopoverAnimated:YES];
    
    // Refresh the table
    [self.tableView reloadData];
}

- (void)addTaskViewControllerCancel:(DTTAddTaskViewController*)controller
{
    // Pop the view controller
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    // Close the popover
    [self.popoverController dismissPopoverAnimated:YES];
    
    // Refresh the table
}

-(DTTDate*)getDateForIndex:(NSInteger)index
{
    // Get the selected date
    NSDate *selectedDate = [self.visibleDates.dates objectAtIndex:index];
    
    // Get the date
    return [DTTDateDataController getDate:selectedDate];
}


- (IBAction)taskLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if(gestureRecognizer.state != UIGestureRecognizerStateRecognized)
    {
        return;
    }
    
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    if (indexPath == nil)
    {
        return;
    }
    
    // Get the selected task
    DTTDate *date = [self getDateForIndex:indexPath.section];
    
    if (indexPath.row >= date.tasks.count)
    {
        // Didn't select a valid task
        return;
    }
    
    // Log the long press
    [ASKPManager addEventWithName:@"TaskLongPress"];
    
    DTTTask *task = [date.tasks objectAtIndex:indexPath.row];
    
    // Show the display action controller
    self.taskMoveOptionsController = [[DTTTaskMoveOptionsController alloc]init];
    self.taskMoveOptionsController.delegate = self;
    [self.taskMoveOptionsController showTaskMoveOptions:task forDate:date];
}

- (IBAction)share:(id)sender
{
    // Log the share
    [ASKPManager addEventWithName:@"Share"];
    
    // Generate a string for each day
    NSMutableArray *items = [[NSMutableArray alloc]init];
    for (NSDate *date in self.visibleDates.dates)
    {
        // Generate the description for each day and add to the array
        [items addObject:[[DTTDateDataController getDate:date] generateDescription]];
    }
    
    // Present the view controller
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
    
    // Display the popover appropriately - Popover for iPad, modally for other devices
    if ([DTTDeviceHelper isIpad])
    {
        // Create popover and display
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:activityViewController];
        self.popoverController.popoverContentSize = CGSizeMake(380, 260);
        [self.popoverController presentPopoverFromBarButtonItem:self.shareButtonItem permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else
    {
        [self presentViewController:activityViewController animated:YES completion:nil];

    }
}

-(void)shareControllerComplete
{
    
}

-(void)taskMoveOptionsComplete:(DTTTaskMoveOptionsController *)controller
{
    
    // Refresh the data
    [self.tableView reloadData];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Don't do this for iPhones
    if (![DTTDeviceHelper isIpad])
    {
        return;
    }
    
    // Get the question response
    DTTDate *date = [self getDateForIndex:indexPath.section];
   
    // Don't do if this is a proper row
    if (indexPath.row < date.tasks.count)
    {
        return;
    }
    
    // Create the view controller
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPadStoryboard"
                                                             bundle: nil];
    DTTAddTaskViewController *viewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"AddTask"];
    viewController.date = date;
    viewController.delegate = self;
        
        // Create popover and display
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:viewController];
        self.popoverController.popoverContentSize = CGSizeMake(350, 500);
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [self.popoverController presentPopoverFromRect:cell.bounds inView:cell.contentView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

}
@end
