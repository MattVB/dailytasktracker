//
//  DTTPreviousTasksListProvider.h
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 3/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DTTPreviousTasksListProvider : NSObject

+(NSArray*)getPreviousTasksList:(NSDate*)relativeDate;

@end
