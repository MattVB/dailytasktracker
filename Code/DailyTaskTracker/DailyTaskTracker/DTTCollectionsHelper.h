//
//  DTTCollectionsHelper.h
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 2/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DTTCollectionsHelper : NSObject

+ (void)moveObjectInArray:(NSMutableArray*)array fromIndex:(NSUInteger)from toIndex:(NSUInteger)to;

@end
