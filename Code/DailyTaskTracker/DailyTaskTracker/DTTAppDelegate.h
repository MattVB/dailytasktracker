//
//  DTTAppDelegate.h
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 1/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DTTAppDelegate : UIApplication <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property NSTimer *idleTimer;

@end
