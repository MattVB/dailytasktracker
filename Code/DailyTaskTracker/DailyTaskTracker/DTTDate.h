//
//  DTTDate.h
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 1/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTTTask.h"

@interface DTTDate : NSObject <NSCoding>

    @property (nonatomic, copy)NSDate *date;
    @property (nonatomic)NSMutableArray *tasks;
    @property (nonatomic)NSMutableArray *deletedCalendarEventIDs;

    -(id)initWithDate:(NSDate*)inputDate;

    -(void)deleteTask:(DTTTask*)task;

    -(void)addCalendarIdToDeletedList:(NSString*)calendarID;

    -(void)removeTaskWithCalendarID:(NSString*)calendarID;

    -(NSString*)generateDescription;

@end
