//
//  DTTDate.m
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 1/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import "DTTDate.h"
#import "DTTDateHelper.h"

@implementation DTTDate

    @synthesize date = _date;
    @synthesize tasks = _tasks;
    @synthesize deletedCalendarEventIDs = _deletedCalendarEventIDs;

-(id)initWithDate:(NSDate *)inputDate
{
    self = [super init];
    if (self) {
        self.date = inputDate;
        self.tasks = [[NSMutableArray alloc]init];
        self.deletedCalendarEventIDs = [[NSMutableArray alloc]init];
        
        return self;
    }
    
    return nil;
}

-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.date forKey:@"date"];
    [aCoder encodeObject:self.tasks forKey:@"tasks"];
    [aCoder encodeObject:self.deletedCalendarEventIDs forKey:@"deletedCalendarEventIDs"];
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self.date = [aDecoder decodeObjectForKey:@"date"];
    self.tasks = [aDecoder decodeObjectForKey:@"tasks"];
    self.deletedCalendarEventIDs = [aDecoder decodeObjectForKey:@"deletedCalendarEventIDs"];
    
    return self;
}

-(void)deleteTask:(DTTTask *)task
{
    // Remove the task
    [self.tasks removeObject:task];
    
    // Add to the deleted calendar ID list
    [self addCalendarIdToDeletedList:task.calendarID];
}

-(void)addCalendarIdToDeletedList:(NSString *)calendarID
{
    // Check if there was a calendar ID
    if (calendarID && ![calendarID isEqualToString:@""])
    {
        if (![self.deletedCalendarEventIDs containsObject:calendarID])
        {
            // Add this to the deleted calendar list
            [self.deletedCalendarEventIDs addObject:calendarID];
        }
    }
}

-(void)removeTaskWithCalendarID:(NSString *)calendarID
{
    NSMutableArray *toDelete = [[NSMutableArray alloc]init];
    
    // Find any tasks that match the calendar ID
    for(DTTTask *task in self.tasks)
    {
        if ([calendarID isEqualToString:task.calendarID])
        {
            [toDelete addObject:task];
        }
    }
    
    // Delete any found items
    for (DTTTask *task in toDelete)
    {
        [self deleteTask:task];
    }
}

-(NSString*)generateDescription
{
    // Include the formatted date
    NSString *result = [DTTDateHelper formatLongDateNonRelative:self.date];
    result = [result stringByAppendingString:@"\n"];
    
    // Add each task into the result
    for (DTTTask *task in self.tasks)
    {
        // Add in each task with the extended description if it is set
        if ([task.extendedDescription isEqualToString:@""])
        {
            result = [NSString stringWithFormat:@"%@ - %@ \n", result, task.description];
        }
        else
        {
            result = [NSString stringWithFormat:@"%@ - %@ (%@) \n", result, task.description, task.extendedDescription];
        }
    }
    
    return result;
}

@end
