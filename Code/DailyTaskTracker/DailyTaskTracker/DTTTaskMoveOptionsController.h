//
//  DTTTaskMoveOptionsController.h
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 12/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTTTask.h"
#import "DTTDate.h"

@protocol DTTTaskMoveOptionsControllerDelegate;

@interface DTTTaskMoveOptionsController : NSObject <UIAlertViewDelegate>

    -(void)showTaskMoveOptions:(DTTTask*)task forDate:(DTTDate*)date;
    @property (strong, nonatomic) id <DTTTaskMoveOptionsControllerDelegate> delegate;
    @property(weak, nonatomic) DTTDate *date;
@property (weak, nonatomic) DTTTask *task;

@end


@protocol DTTTaskMoveOptionsControllerDelegate <NSObject>
- (void)taskMoveOptionsComplete:(DTTTaskMoveOptionsController*)controller;
@end