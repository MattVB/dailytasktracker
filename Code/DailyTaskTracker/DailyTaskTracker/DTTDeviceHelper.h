//
//  DTTDeviceHelper.h
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 15/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DTTDeviceHelper : NSObject

+(BOOL)isIpad;


@end
