//
//  DTTSingleDayViewController.h
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 10/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTTSelectDateViewController.h"
#import "DTTDetailViewController.h"
#import "DTTAddTaskViewController.h"
#import "DTTTaskMoveOptionsController.h"

@interface DTTSingleDayViewController : UIViewController <UITableViewDataSource, DTTSelectDateViewControllerDelegate, DTTDetailViewControllerDelegate, DTTAddTaskViewControllerDelegate, DTTTaskMoveOptionsControllerDelegate, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *labelDate;
- (IBAction)changeDate:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)displayDateHelp:(id)sender;
- (IBAction)nextDay:(id)sender;
- (IBAction)previousDay:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *shareButton;
- (IBAction)share:(id)sender;

@end
