//
//  DTTControlHelper.h
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 1/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DTTControlHelper : NSObject

+(void)formatTextView:(UITextView*)textView;

@end
