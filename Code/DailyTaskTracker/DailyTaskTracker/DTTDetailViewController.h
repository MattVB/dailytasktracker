//
//  DTTDetailViewController.h
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 1/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTTTask.h"
#import "DTTDate.h"
#import "DTTTaskMoveOptionsController.h"

@protocol DTTDetailViewControllerDelegate;


@interface DTTDetailViewController : UIViewController <UIActionSheetDelegate, UIAlertViewDelegate, DTTTaskMoveOptionsControllerDelegate>

@property (weak, nonatomic) id <DTTDetailViewControllerDelegate> delegate;

@property (strong, nonatomic) DTTTask* task;
@property (strong, nonatomic) DTTDate* date;

@property (weak, nonatomic) IBOutlet UITextField *textDescription;
@property (weak, nonatomic) IBOutlet UITextView *textViewExtendedDescription;
- (IBAction)save:(id)sender;
- (IBAction)displayTaskActions:(id)sender;

- (IBAction)trashTask:(id)sender;
@end

@protocol DTTDetailViewControllerDelegate <NSObject>
- (void)detailViewControllerSaveComplete:(DTTDetailViewController*)controller task:(DTTTask*)task date:(DTTDate*)date;

- (void)detailViewControllerDeletionComplete:(DTTDetailViewController*)controller task:(DTTTask*)task date:(DTTDate*)date;

- (void)detailViewControllerMovementComplete:(DTTDetailViewController*)controller task:(DTTTask*)task date:(DTTDate*)date;
@end
