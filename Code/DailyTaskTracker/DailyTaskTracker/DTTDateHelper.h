//
//  DTTDateHelper.h
//  DailyTaskTracker
//
//  Created by Matthew van Boheemen on 2/09/12.
//  Copyright (c) 2012 Matthew van Boheemen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DTTDateHelper : NSObject

+(NSDate*)dateOnlyFromDate:(NSDate*) date;

+(NSString*)formatLongDate:(NSDate*)date;

+(NSString*)formatLongDateNonRelative:(NSDate*)date;

+(NSString*)formatShortDateNonRelative:(NSDate*)date;

+(NSString*)formatTime:(NSDate*)date;

+(NSDate*)getDateRelativeTo:(NSDate*)date adjustedByDays:(NSInteger)days;

+(BOOL)isDayAWeekend:(NSDate*)date;

@end
